
export interface RESTProductResponse {
    ok:       boolean;
    msg:      string;
    product: Product;
}

export interface RESTProductsPaginationResponse {
    ok:       boolean;
    msg:      string;
    pagination: Pagination;
}
export interface RESTCategoryResponse {
    ok:         boolean;
    msg:        string;
    categories: Category[];
}

export interface Pagination {
    current_page:   number;
    data:           Product[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    links:          Link[];
    next_page_url:  string;
    path:           string;
    per_page:       number;
    prev_page_url:  null;
    to:             number;
    total:          number;
}
export interface Product {
    id:          number;
    name:        string;
    description: string;
    price:       string;
    model:       string;
    sat_code:    string;
    sold:        number;
    stock:       number;
    status:      number;
    deleted_at:  null;
    created_at:  Date;
    updated_at:  Date;
    categories:  Category[];
}

export interface Category {
    id:          number;
    name:        string;
    description: string;
    deleted_at:  null;
    created_at:  Date;
    updated_at:  Date;
}
export interface Link {
    url:    null | string;
    label:  string;
    active: boolean;
}