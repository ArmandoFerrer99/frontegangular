import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { ListComponent } from './pages/list/list.component';
import { ShowComponent } from './pages/show/show.component';
import { CreateComponent } from './pages/create/create.component';
import { EditComponent } from './pages/edit/edit.component';

const routes: Routes = [
  {
    path:'',
    component: MainComponent,
    children:[
      {path: '',component: ListComponent},
      {path: 'create',component: CreateComponent},
      {path: ':id',component: ShowComponent},
      {path: ':id/edit',component: EditComponent},
      {path: '**', redirectTo:''}
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
