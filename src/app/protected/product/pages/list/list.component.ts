import { Component, OnInit } from '@angular/core';
import { Link } from 'src/app/interfaces/interfaces';
import { Product } from '../../interfaces/interfaces';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  products: Product[] = [];
  hayError: boolean = false;
  links: Link[] = [];
  limit: number = 10;
  page: number = 1;
  constructor(
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.lista(1);
  }
  lista(page:number){
    this.page = page;
    if(page>0 && page<=this.limit){
    this.productService.list(page)
      .subscribe((productPaginationResponse) =>{
        if(productPaginationResponse.ok){
          this.hayError = false;

          this.products = productPaginationResponse.pagination.data;
          this.links = productPaginationResponse.pagination.links;
          this.links.splice(0,1);
          this.links.splice(-1,1);
          this.limit = productPaginationResponse.pagination.links.length;
        }else{
          this.hayError = true;

        }
      },(err)=>{
        this.hayError = true;
        this.products = [];
      });
    }
  }
}
