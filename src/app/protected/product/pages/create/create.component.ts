import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { Category } from '../../interfaces/interfaces';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  categories: Category[] = [];
  hayError: boolean = false;
  categoriesSelected: string[] = [];
  value: any;
  position!: any;
  miFormulario: FormGroup = this.fb.group({
    name: ['',[Validators.required, ]],
    description: ['',[Validators.required]],
    price: ['',[Validators.required]],
    model: ['',[Validators.required]],
    sat_code: ['',[Validators.required]],
    sold: [0,[Validators.required]],
    stock: [0,[Validators.required]],
    status: [false,[Validators.required]],
    categories_id: [this.categoriesSelected,[Validators.required]],
  });
  constructor(
    public router: Router,

    private fb: FormBuilder,
    private productService: ProductService
  ) { 

  }

  ngOnInit(): void {
    this.categoriesList();
  }
  categoriesList(){
    this.productService.categories()
    .subscribe((categoryResponse)=>{
      if(categoryResponse.ok){
        this.categories = categoryResponse.categories;
        this.hayError = false;
      }else{
        this.hayError = true;

      }
    },(err)=>{
      this.hayError = true;
      this.categories = [];
    })
  }
  
  //checkbox de categorias
  onCheckboxChange(event: any) {
    if (event.target.checked) {
      this.categoriesSelected.push(event.target.value.toString());
    } else {
      for (let i = 0; i < this.categoriesSelected.length; i++) {
        if(this.categoriesSelected[i] == event.target.value.toString()){
          this.categoriesSelected.splice(i,1);
        }
      }
    }
  }

  create(){

    this.productService.create(this.miFormulario.value).then((response: any) => {
      setTimeout(() => {
        if(response.ok){
          this.router.navigate(['project']).then(() => {
            
            window.location.reload();
          });
        }else{
          
        }
      }, 750);
    })
    .catch(err => {

    })
  }
}
