import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { Product } from '../../interfaces/interfaces';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {
  product!: Product;
  constructor(
    private activadedRoute: ActivatedRoute,
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.activadedRoute.params
      .pipe(
        switchMap( ( param ) => this.productService.show(param.id)),
        tap(console.log)
      )
      .subscribe( product => this.product = product.product);
  }
  
  
}
