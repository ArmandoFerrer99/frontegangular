import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { MainComponent } from './pages/main/main.component';
import { CreateComponent } from './pages/create/create.component';
import { ListComponent } from './pages/list/list.component';
import { ShowComponent } from './pages/show/show.component';
import { TableComponent } from './components/table/table.component';
import { ComponentsModule } from '../../components/components.module';
import { EditComponent } from './pages/edit/edit.component';
import { CardComponent } from './components/card/card.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    MainComponent,
    CreateComponent,
    ListComponent,
    ShowComponent,
    TableComponent,
    EditComponent,
    CardComponent
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,

  ]
})
export class ProductModule { }
