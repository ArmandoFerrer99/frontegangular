import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RESTProductsPaginationResponse, RESTProductResponse, Product, RESTCategoryResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private baseUrl: string = environment.baseUrl;
  constructor(
    private http: HttpClient,
  ) { }

  list(page:number){
    const url = `${this.baseUrl}/products`;
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')|| ''}`
      });
      if(page == 0 ){
        return this.http.get<RESTProductsPaginationResponse>(url, { headers});
      }
      const params = new HttpParams()
        .set('page',page);
      return this.http.get<RESTProductsPaginationResponse>(url, { headers,params});
  }
  show(id:number){
    const url = `${this.baseUrl}/products/`+id;
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')|| ''}`
      });
      return this.http.get<RESTProductResponse>(url, { headers});
  }
  create(product: Product){
    const url = `${this.baseUrl}/products/`;
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')|| ''}`
      });
      return this.http.post<RESTProductResponse>(url, product,{headers}).toPromise();

  }
  categories(){
    const url = `${this.baseUrl}/categories`;
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')|| ''}`
      });

      return this.http.get<RESTCategoryResponse>(url, { headers});

  }
}
