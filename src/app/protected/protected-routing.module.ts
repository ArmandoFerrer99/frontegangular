import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {path:'',component:ProfileComponent},
      {
        path: 'datos-personales',
        component:ProfileComponent
      },
      {
        path: 'productos',
        loadChildren: () => import('./product/product.module').then( m=> m.ProductModule)
      },
      {path: '**', redirectTo: ''}
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtectedRoutingModule { }
