import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/services/auth.service';
import { tap } from 'rxjs/operators';
import { ValidateTokenService } from '../services/validate-token.service';

@Injectable({
  providedIn: 'root'
})
export class ValidateTokenGuard implements CanActivate {
  constructor(private validateToken: ValidateTokenService,
    private router: Router){
      
    }
    async canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Promise<boolean> {
      
      const authenticated = await this.validateToken.checkAuthenticationAsPromise();
      
      if (!authenticated) {
        this.router.navigate(['/login']);
      }
      return authenticated;
    }
  
}
