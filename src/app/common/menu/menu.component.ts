import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/services/auth.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  get usuario(){
    return this.authService.usuario;
  }
  
  constructor
    (
      private router: Router,
      private authService: AuthService
    ) { }
  ngOnInit(): void {
    // console.log(this.usuario);
  }

  logout(){
    this.router.navigateByUrl('/auth');
    this.authService.logout();
  }
}
