import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from './pagination/pagination.component';
import { VprComponent } from './templates/vpr/vpr.component';



@NgModule({
  declarations: [
    PaginationComponent,
    VprComponent
  ],
  imports: [
    CommonModule,
    
  ],
  exports:[
    PaginationComponent
  ]
})
export class ComponentsModule { }
