import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Link } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Input() links: Link[] = [];
  @Input() page!: number;
  @Output() onClick   : EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }
  lista(id: number){
    this.onClick.emit(id);
  }
}
