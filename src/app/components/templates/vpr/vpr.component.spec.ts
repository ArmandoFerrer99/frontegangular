import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VprComponent } from './vpr.component';

describe('VprComponent', () => {
  let component: VprComponent;
  let fixture: ComponentFixture<VprComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VprComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VprComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
