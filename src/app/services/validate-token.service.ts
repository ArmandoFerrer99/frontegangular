import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of} from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../auth/interfaces/user';
import { ValidateToken } from '../interfaces/validate-token';
import { AuthService } from '../auth/services/auth.service';
import { AuthResponse } from '../auth/interfaces/auth-response';

@Injectable({
  providedIn: 'root'
})
export class ValidateTokenService {
  private baseUrl: string = environment.baseUrl;
  public _validate : boolean = false;
  private usuario!: User;
  constructor( 
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
    ) { }
    get validate(){
      return this._validate;
    } 

  public checkAuthenticationAsPromise(): Promise<boolean> {
    const url = `${this.baseUrl}/validate-token`;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')|| ''}`
    })
    return new Promise<boolean>((resolve, reject) => {
      setTimeout(() => {
        this.http.get<AuthResponse>(url, { headers}).subscribe( (respuesta) => {
          
          this._validate = respuesta.ok;
          this.usuario = {
            name: respuesta.name!,
            id: respuesta.id!,
            role_id: respuesta.role_id!
          }
          this.authService.setUsuario(this.usuario);
          resolve(respuesta.ok);

        },(err)=>{
          resolve(false);
        }); 
        
      }, 300)
    });
  }
  
}
