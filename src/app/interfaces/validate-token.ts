export interface ValidateToken {
    ok: boolean,
    msg?: string
}
