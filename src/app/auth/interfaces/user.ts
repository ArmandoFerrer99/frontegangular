export interface User {
    id: string;
    name: string;
    role_id: string;
}
