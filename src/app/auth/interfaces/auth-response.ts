export interface AuthResponse {
    ok: boolean,
    id ?: string,
    name ?: string,
    role_id ?: string,
    token ?: string,
    msg?: string
}
