import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {catchError, map, tap} from 'rxjs/operators'
import { Observable, of} from 'rxjs';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';
import { AuthResponse } from '../interfaces/auth-response';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl: string = environment.baseUrl;
  private _usuario!: User;
  get usuario(){
    return {...this._usuario};
  } 

  constructor( 
    private http: HttpClient,
    private router: Router
    ) { }

    setUsuario(usuario:User){
      this._usuario = usuario;
    }
  register( name:string, email: string, password:string ){
    const url = `${this.baseUrl}/register`;
    const body = { name, email, password }
    return this.http.post<AuthResponse>(url, body)
    .pipe(
      tap( ({ ok,token,name,id,role_id }) =>{
        if( ok ){
          localStorage.setItem('token',token!);
          this._usuario = {
            name: name!,
            id: id!,
            role_id: role_id!
          }
        }
      } ),
      map( resp => resp.ok ),
      catchError(err => of(err.error.msg) )
    );
  }

  login( email: string, password:string ){
    const url = `${this.baseUrl}/login`;
    const body = { email, password }
    return this.http.post<AuthResponse>(url, body)
    .pipe(
      tap( resp =>{
        if( resp.ok ){
          localStorage.setItem('token',resp.token!);
          this._usuario = {
            name: resp.name!,
            id: resp.id!,
            role_id: resp.role_id!
          }
        }
      } ),
      map( resp => resp.ok ),
      catchError(err => of(err.error.msg) )
    );
  }

  
  logout(){
    this._usuario = {
      name: '',
      id: '',
      role_id: ''
    }
    localStorage.removeItem('token');
  }
}