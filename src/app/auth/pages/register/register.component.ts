import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent{
  miFormulario: FormGroup = this.fb.group({
    name:[
      '',
      [
        Validators.required,
        Validators.minLength(4)
      ]
    ],
    email: 
    ['test1@test.com',
      [
        Validators.required, 
        Validators.email,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
      ]
    ],
    recaptcha:['',Validators.required],
    password: ['',[Validators.required, Validators.minLength(6)]]
  });
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService) { }
  register(){

    const {name,email, password} = this.miFormulario.value;
   this.authService.register(name,email,password)
   .subscribe(ok =>{
     if(ok === true){
          this.router.navigateByUrl('/dashboard');
     }else{
     }
   });
  }

}