import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  {
  miFormulario: FormGroup = this.fb.group({
    email: 
    ['edgartorres@novusred.com',
      [
        Validators.required, 
        Validators.email,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
      ]
    ],
    password: ['12341234',[Validators.required, Validators.minLength(6)]]
  });
  valida: boolean = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    ) {

     }

 login(){
   const {email, password} = this.miFormulario.value;
   this.authService.login(email,password)
   .subscribe(ok =>{
     if(ok === true){
          this.router.navigateByUrl('/dashboard');

     }else{
     }
   });

 }


}