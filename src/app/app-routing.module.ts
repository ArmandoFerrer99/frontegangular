import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ValidateTokenGuard } from './guards/validate-token.guard';
import { ValidateNoTokenGuard } from './guards/validate-no-token.guard';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { ViewproductsComponent } from './components/templates/viewproducts/viewproducts.component';
import { VprComponent } from './components/templates/vpr/vpr.component';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then( m=> m.AuthModule),
    canActivate: [ValidateNoTokenGuard]
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./protected/protected.module').then( m=> m.ProtectedModule),
    canActivate: [ ValidateTokenGuard ],
  },
  {
    path: 'nosotros', component: NosotrosComponent
   },
   {
    path: 'viewproducts', component: ViewproductsComponent
   },
   {
    path: 'homevp', component: VprComponent
   },
   {
    path: '**',
    redirectTo: 'auth'
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
