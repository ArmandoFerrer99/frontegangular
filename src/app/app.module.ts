import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './common/footer/footer.component';
import { MenuComponent } from './common/menu/menu.component';
import { ProfileComponent } from './protected/profile/profile.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    MenuComponent,
    ProfileComponent,
    NosotrosComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
